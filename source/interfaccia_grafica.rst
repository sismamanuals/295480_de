###################
INTERFACCIA GRAFICA
###################
PRISMA è in grado di controllare tutti i diversi sistemi di marcatura prodotti da Sisma, tenendo conto sia degli accessori che possono essere collegati che delle più diverse esigenze di utilizzo.

Le informazioni possono essere disposte, all'interno della finestra, in base alle esigenze dell'utente.

Per fare questo premere con l'icona del mouse sulla barra contenente il nome della finestra e muoverla. Appariranno dei punti sensibili, evidenziati nella :numref:`interfaccia_grafica_ancoraggi` con delle frecce rosse. Spostando l'icona del mouse sopra questi punti, si illumina la zona dove la finestra verrà posizionata, a lato della finestra principale.

Spostandosi sopra ad una finestra esistente apparirà una icona, evidenziata nella :numref:`interfaccia_grafica_ancoraggi` con la freccia gialla, per posizionare le due finestre una sopra l'altra o affiancate nelle 4 direzioni.

.. _interfaccia_grafica_ancoraggi:
.. figure:: _static/interfaccia_grafica_ancoraggi.png
   :width: 14 cm
   :align: center

   Procedura di spostamento ed ancoraggio


**********************
INTERFACCIA PRINCIPALE
**********************
Per un'analisi più dettagliata l'interfaccia può essere suddivisa in 4 parti:

1. Menu e barra degli strumenti;
2. Barra di stato;
3. Area di lavoro;
4. Finestre per gestione funzionalità.

L'ultimo punto verrà esteso su una sezione dedicata vista la numerosità di elementi che coinvolgono.

.. _interfaccia_grafica_principale:
.. figure:: _static/interfaccia_grafica_principale.png
   :width: 14 cm
   :align: center

   Interfaccia principale


Menù e barra degli strumenti
============================
Nella parte superiore della finestra dell'applicazione si trovano il :guilabel:`menù` e la :guilabel:`barra degli strumenti`.

Il :guilabel:`menù` contiene il tutti i comandi per la gestione del progetto come creazione, salvataggio, aggiunta file, ecc...; si trovano poi i collegamenti per accedere alle finestre di impostazione per applicazione, hardware e procedure di lavorazione.

La :guilabel:`barra degli strumenti` mette invece a disposizione i comandi più comunemente utilizzati per il progetto e la marcatura.

Barra di stato
==============
Nella parte inferiore dell'interfaccia si trova la Barra di stato. In questa zona vengono fornite le informazioni riguardanti lo stato della macchina e poi sono presenti alcuni controlli legati all'area di lavoro.

Continuando sulla barra di stato, sono presenti alcune icone che indicano lo stato di funzionamento/allarme.
Se la macchina è dotata di assi per lo spostamento della testa in questa zona vengono visualizzate le posizioni attuali. Questo vale sia per il normale Asse Z sia per gli azionamenti 3D e mandrino. Vi è, inoltre, anche il pulsante per eseguire la procedura di azzeramento.

Area di lavoro
==============
Nell'area di lavoro viene mostrato un quadrato che copre la massima estensione di marcatura possibile. In questa area possono essere depositati tutti gli elementi che si intende marcare. Quanto mostrato è esattamente il progetto che il marcatore andrà ad eseguire. Nella parte superiore sono presenti dei comandi che hanno effetto sugli elementi grafici che fanno parte del progetto; nella parte inferiore ci sono i comandi che agiscono sulle proprietà del fogli di lavoro. In centro si trova invece la zona bianca che rappresenta lo spazio di marcatura, contornato dai due righelli.

Si ha la possibilità di posizionare ogni singolo file direttamente con il mouse. Nell'area di lavoro si possono depositare più file. Appena si seleziona un elemento, compaiono dei punti sensibili che permettono di modificare l'angolo di rotazione, la scala, e le deformazioni lungo X e Y. È poi possibile modificare il colore di bordo e riempimento di ogni singolo elemento. In questo modo si modifica l'associazione al file delle diverse parametrizzazioni per la marcatura. Questi aspetti saranno trattai in dettaglio nella **mettere riferimento incrociato**

Sulla parte superiore è presente una piccola barra degli strumenti dove sono presenti tutti i comandi relativi ai file grafici. Ogni comando è accompagnato da una descrizione testuale che compare al passaggio del mouse.

Finestre per gestione funzionalità
==================================
L'ultima parte sono le finestre di supporto.
Qui si trovano tutte le funzioni del software di marcatura.

* La finestra principale è :guilabel:`Struttura Progetto`. Qui sono mostrati tutti gli elementi che compongono il progetto. Il progetto è un contenitore di file e impostazioni, che consentono di produrre la marcatura attesa.
* La finestra :guilabel:`Proprietà` mostra tutti i settaggi generali riguardanti il file grafico o il testo selezionato (esempio: larghezza, altezza, rotazione, proporzioni, scale X-Y, font, contenuto, contatori ect.).
* La finestra :guilabel:`Laser` è legata essenzialmente alla macchina e contiene le impostazioni generali di marcatura (per esempio: informazioni sui settaggi ed i livelli usati, informazioni sul tempo di marcatura, definizione della modalità di esecuzione della marcatura, centratura e rosso, marcatura e conteggio pezzi).
* La finestra :guilabel:`Livelli` permettere il controllo ed il settaggio dei parametri laser di marcatura. Il set di parametri di marcatura (10 livelli disponibili per ogni set) è poi salvato in una configurazione, che può essere successivamente richiamata e/o modificata.
* La finestra :guilabel:`Assi 3D` (ove presente) permette di settare e gestire il posizionamento dei 4 assi (X-Y-Z e asse rotante) nelle rispettive 3 direzioni nello spazio e lavorazioni con posizionamenti attorno ad un asse.
